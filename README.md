# Simple Lambda Serverless API

This project contains source code and supporting files for a serverless application that you can deploy with the SAM CLI. It includes the following files and folders.

- clock - Code for the application's Lambda function.
- convert-time - Code for the application's Lambda function.
- events - Invocation events that you can use to invoke the function.
- template.yaml - A template that defines the application's AWS resources.

The application uses several AWS resources, including Lambda functions and an API Gateway API. These resources are defined in the `template.yaml` file in this project. You can update the template to add AWS resources through the same deployment process that updates your application code.

## Deploy the sample application

The Serverless Application Model Command Line Interface (SAM CLI) is an extension of the AWS CLI that adds functionality for building and testing Lambda applications. It uses Docker to run your functions in an Amazon Linux environment that matches Lambda. It can also emulate your application's build environment and API.

To use the SAM CLI, you need the following tools.

* SAM CLI - [Install the SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
* Node.js - [Install Node.js 10](https://nodejs.org/en/), including the NPM package management tool.
* Docker - [Install Docker community edition](https://hub.docker.com/search/?type=edition&offering=community)

To build and deploy your application for the first time, run the following commands in your shell:

1. Create the bucket
   ```sh
   aws s3 --region <YOUR-REGION> mb s3://<UNIQUE BUCKET NAME HERE> 
   ```

2. Check your created Bucket
   ```sh
   aws s3 ls
   ```

3. Build SAM Application
   ```sh
   sam build
   ```

4. Package Application
   ```sh
   sam package --template-file template.yaml --output-template-file packaged.yml --s3-bucket <NAME-OF-THE-BUCKET>
   ```

5. Deploy Application
   ```sh
   sam deploy --region <YOUR-REGION> --capabilities CAPABILITY_IAM --template-file <PREVIOUS-PACKAGE-FILE> --stack-name <YOUR-STACK-NAME>
   ```

You can find your API Gateway Endpoint URL in the output values displayed after deployment.

The SAM CLI reads the application template to determine the API's routes and the functions that they invoke. The `Events` property on each function's definition includes the route and method for each path.

```yaml
      Events:
        ClockFunction:
          Type: Api
          Properties:
            Path: /clock
            Method: get
      Events:
        ConvertTimeFunction:
          Type: Api
          Properties:
            Path: /convert-time
            Method: get
```

## Cleanup

To delete the sample application that you created, use the AWS CLI. Assuming you used your project name for the stack name, you can run the following:

```bash
aws cloudformation delete-stack --stack-name <YOUR-STACK-NAME>
```

## Resources

See the [AWS SAM developer guide](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html) for an introduction to SAM specification, the SAM CLI, and serverless application concepts.

Next, you can use AWS Serverless Application Repository to deploy ready to use Apps that go beyond hello world samples and learn how authors developed their applications: [AWS Serverless Application Repository main page](https://aws.amazon.com/serverless/serverlessrepo/)
